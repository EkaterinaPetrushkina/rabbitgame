import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class GameField extends JPanel implements ActionListener {

    private final int FIELD_SIZE = 320;
    private final int DOT_SIZE = 16;
    private final int ALL_DOTS = 400;
    private Image rabbit;
    private Image carrot;
    private Image poop;
    private int rabbitX;
    private int rabbitY;
    private int carrotX;
    private int carrotY;
    private int[] poopXList = new int[ALL_DOTS / 2];
    private int[] poopYList = new int[ALL_DOTS / 2];
    private int score;
    private int index = 0;
    private Timer timer;
    private boolean inGame = true;
    private boolean left = false;
    private boolean right = true;
    private boolean up = false;
    private boolean down = false;

    public GameField() {
        setBackground(Color.BLACK);
        loadImages();
        initGame();
        addKeyListener(new Keyboard());
        setFocusable(true);
    }

    public void initGame() {
        score = 0;
        rabbitX = 48;
        rabbitY = 48;
        timer = new Timer(250, this);
        timer.start();
        createCarrot();
    }

    public void createCarrot() {
        int randomX, randomY;
        do {
            randomX = new Random().nextInt(20) * DOT_SIZE;
            randomY = new Random().nextInt(20) * DOT_SIZE;
        } while (checkValue(randomX, randomY, poopXList, poopYList));
        carrotX = randomX;
        carrotY = randomY;
    }

    private static boolean checkValue(int randomX, int randomY, int[] poopXList, int[] poopYList) {
        for (int i = 0; i < poopXList.length; i++) {
            if (randomX == poopXList[i] && randomY == poopYList[i]) {
                return true;
            }
        }
        return false;
    }

    public void createPoop() {
        if (left) {
            poopXList[index] = rabbitX + DOT_SIZE;
            poopYList[index] = rabbitY;
        }
        if (right) {
            poopXList[index] = rabbitX - DOT_SIZE;
            poopYList[index] = rabbitY;
        }
        if (up) {
            poopYList[index] = rabbitY + DOT_SIZE;
            poopXList[index] = rabbitX;
        }
        if (down) {
            poopYList[index] = rabbitY - DOT_SIZE;
            poopXList[index] = rabbitX;
        }
        index++;
    }

    public void loadImages() {

        ImageIcon imageIconForCarrot = new ImageIcon("src/images/carrot.png");
        carrot = imageIconForCarrot.getImage();

        ImageIcon imageIconForRabbit = new ImageIcon("src/images/rabbit.png");
        rabbit = imageIconForRabbit.getImage();

        ImageIcon imageIconForPoop = new ImageIcon("src/images/poop.png");
        poop = imageIconForPoop.getImage();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (inGame) {
            checkCarrot();
            checkPoop();
            checkCollisions();
            move();
        }
        repaint();
    }

    public void checkCollisions() {
        if (rabbitX > FIELD_SIZE || rabbitY > FIELD_SIZE ||
                rabbitX < 0 || rabbitY < 0) {
            inGame = false;
        }
    }

    public void checkCarrot() {
        if (rabbitX == carrotX && rabbitY == carrotY) {
            score++;
            createCarrot();
            if (score % 10 == 0 && score != 0) {
                createPoop();
            }
        }
    }

    public void checkPoop() {
        for (int i = 0; i < poopXList.length; i++) {
            if (rabbitX == poopXList[i] && rabbitY == poopYList[i]) {
                inGame = false;
                break;
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (inGame) {
            g.drawImage(carrot, carrotX, carrotY, this);
            g.drawImage(rabbit, rabbitX, rabbitY, this);
            for (int i = 0; i < poopXList.length; i++) {
                g.drawImage(poop, poopXList[i], poopYList[i], this);
            }

            String scoreResult = "Score: " + score;
            Font scoreFont = new Font("Times New Roman", Font.BOLD, 15);
            g.setColor(Color.WHITE);
            g.setFont(scoreFont);
            g.drawString(scoreResult, 200, 20);
        } else if (index > poopXList.length) {
            String theEnd = "WIN";
            Font endFont = new Font("Times New Roman", Font.BOLD, 40);
            g.setColor(Color.GREEN);
            g.setFont(endFont);
            g.drawString(theEnd, 50, FIELD_SIZE / 2);
        } else {
            String theEnd = "Game Over";
            Font endFont = new Font("Times New Roman", Font.BOLD, 40);
            g.setColor(Color.RED);
            g.setFont(endFont);
            g.drawString(theEnd, 50, FIELD_SIZE / 2);
        }
    }

    public void move() {

        if (left) {
            rabbitX -= DOT_SIZE;
        }
        if (right) {
            rabbitX += DOT_SIZE;
        }
        if (up) {
            rabbitY -= DOT_SIZE;
        }
        if (down) {
            rabbitY += DOT_SIZE;
        }
    }

    class Keyboard extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            int key = e.getKeyCode();
            if (key == KeyEvent.VK_LEFT && !right) {
                left = true;
                up = false;
                down = false;
            }
            if (key == KeyEvent.VK_RIGHT && !left) {
                right = true;
                up = false;
                down = false;
            }
            if (key == KeyEvent.VK_UP && !down) {
                up = true;
                left = false;
                right = false;
            }
            if (key == KeyEvent.VK_DOWN && !up) {
                down = true;
                left = false;
                right = false;
            }
        }
    }
}


